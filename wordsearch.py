import random

def find_runs(grid, word):
    run_indexes = []
    for run in xrange(len(grid)):
        try:
            run_indexes.append(grid.index(len(word)*'.', run, run+len(word)))
        except ValueError:
            pass
    return run_indexes

def find_line(grid, word):
    good_lines = []
    for line in xrange(len(grid)):
        if '.'*len(word) in ''.join(grid[line]):
            good_lines.append(line)
    return good_lines

def get_column(grid, column):
    col_values = []
    for line in grid:
        col_values.append(line[column])
    return col_values

def find_columns(grid, word):
    good_columns = []
    for col in xrange(len(grid)):
        col_values = []
        col_values = get_column(grid, col)
        if '.'*len(word) in ''.join(col_values):
            good_columns.append(col)
    return good_columns

def get_lr_diagonal(grid, start_row, start_column):
    lr_diagonal_values = []
    row = start_row
    column = start_column
    if row > column:
        for line in xrange(row,len(grid)):
            lr_diagonal_values.append(grid[row][column])
            row += 1
            column += 1
    else:
        for line in xrange(column,len(grid)):
            lr_diagonal_values.append(grid[row][column])
            row += 1
            column += 1
    return lr_diagonal_values

def get_rl_diagonal(grid, start_row, start_column):
    rl_diagonal_values = []
    row = start_row
    column = start_column
    while column >= 0 and row < len(grid):
        rl_diagonal_values.append(grid[row][column])
        row += 1
        column -= 1
    return rl_diagonal_values

def find_diagonals(grid, word, get_diagonal_func):
    good_diagonals = []
    for line in xrange(len(grid)):
        for col in xrange(len(grid)):
            diag_values = get_diagonal_func(grid, line, col)
            if '.'*len(word) in ''.join(diag_values):
                good_diagonals.append([line, col])
    return good_diagonals

class OneDWordsearch(object):

    def __init__(self, size):
        self.words = []
        self.grid = ['.' for num in xrange(size)]

    def add_word(self, word):
        self.words.append(word)
        grid = ''.join(self.grid)
        try:
            chance = random.randint(0,1)
            if chance == 0:
                word = word[::-1]
            good_indexes = find_runs(grid, word)
            offset = random.choice(good_indexes)
            for index in xrange(len(word)):
                self.grid[index+offset] = word[index]
        except IndexError:
            error_message = 'no room for the word "{0}"'.format(word)
            raise ValueError(error_message)

    def _add_random(self):
        for index in range(len(self.grid)):
            if self.grid[index] == '.':
                self.grid[index] = chr(random.randint(97,122))

    def print_puzzle(self): 
        if '.' in self.grid:
            self._add_random()
        print ''.join(self.grid)

class TwoDWordsearch(object):

    def __init__(self, size):
        self.words = []
        self.grid = [list('.'*size) for num in xrange(size)]

    def add_word(self, word, size):

        chance = random.randint(0,1)
        if chance == 0:    
            word = word[::-1]
        self.words.append(word)

        chance2 =  random.randint(0,3)
        line = random.choice(find_line(self.grid, word))
        grid = ''.join(self.grid[line])
        good_indexes = find_runs(grid, word)
        if chance2 == 0:
            offset = random.choice(good_indexes)
            for index in xrange(len(word)):
                self.grid[line][index+offset] = word[index]
        if chance2 == 1:
            column = random.choice(find_columns(self.grid, word))
            replaced_column = get_column(self.grid, column)
            column_indexes = find_runs(''.join(replaced_column), word)
            offset = random.choice(column_indexes)
            for index in xrange(len(word)):
                replaced_column[index+offset] = word[index]
            for line in xrange(len(self.grid)):
                self.grid[line][column] = ''.join(replaced_column[line])
        if chance2 == 2:
            diagonal = random.choice(find_diagonals(self.grid, word, get_lr_diagonal))
            row = diagonal[0]
            column = diagonal[1]
            replaced_diagonal = get_lr_diagonal(self.grid, diagonal[0], diagonal[1])
            for index in xrange(len(word)):
                replaced_diagonal[index] = word[index]
                self.grid[row][column] = replaced_diagonal[index]
                row += 1
                column += 1
        if chance2 == 3:
            diagonal = random.choice(find_diagonals(self.grid, word, get_rl_diagonal))
            row = diagonal[0]
            column = diagonal[1]
            replaced_diagonal = get_rl_diagonal(self.grid, diagonal[0], diagonal[1])
            for index in xrange(len(word)):
                replaced_diagonal[index] = word[index]
                self.grid[row][column] = replaced_diagonal[index]
                row += 1
                column -= 1

    def _add_random(self):
        for line in xrange(len(self.grid)):
            for index in xrange(len(self.grid[line])):
                if self.grid[line][index] == '.':
                    self.grid[line][index] = chr(random.randint(97,122))
        
    def print_puzzle(self):
        # self._add_random()
        for line in self.grid:
            print ' '.join(line)

def oned():
    words = ['fish','butt']
    wordsearch = OneDWordsearch(30)
    [wordsearch.add_word(word) for word in words]
    wordsearch.print_puzzle()
    
    words = ['mangroves']
    wordsearch = OneDWordsearch(50)
    [wordsearch.add_word(word) for word in words]
    wordsearch.print_puzzle()

def twod():
    words = ['wolfhounds', 'fish','butt']
    size = 10
    wordsearch2D = TwoDWordsearch(size)
    [wordsearch2D.add_word(word, size) for word in words]
    wordsearch2D.print_puzzle()


if __name__ == '__main__':
    # oned()
    twod()