import unittest
from wordsearch import (OneDWordsearch, TwoDWordsearch, find_runs, find_line,
                         find_columns, get_lr_diagonal, find_diagonals,
                         get_rl_diagonal)

class TestWordsearch(unittest.TestCase):

    def test_1_word(self):
        wordsearch = OneDWordsearch(5)
        assert len(''.join(wordsearch.grid)) == 5
        wordsearch.add_word('cat')
        assert 'cat' or 'tac' in ''.join(wordsearch.grid)
        assert len(''.join(wordsearch.grid)) == 5

    def test_2_words(self):
        wordsearch = OneDWordsearch(6)
        wordsearch.add_word('cat')
        wordsearch.add_word('go')
        assert 'cat' or 'tac' in ''.join(wordsearch.grid)
        assert 'go' or 'og' in ''.join(wordsearch.grid) 

    def test_space_exception(self):
        wordsearch = OneDWordsearch(1)
        self.assertRaises(ValueError, wordsearch.add_word, 'mississippi')

    def test_find_runs_1D(self):
        grid = 'fgt.....d.....xc.....'
        word = 'fish'
        assert find_runs(grid, word) == [3, 4, 9, 10, 16, 17]

    def test_add_random(self):
        wordsearch = OneDWordsearch(8)
        assert wordsearch.grid == ['.','.','.','.','.','.','.','.']
        wordsearch.grid[2] = 'e'
        wordsearch.grid[6] = 'q'
        wordsearch._add_random()
        # check to make sure random didn't overwrite your words
        assert wordsearch.grid[2] == 'e'
        assert wordsearch.grid[6] == 'q'
        # check to make sure there are no more dots
        assert '.' not in wordsearch.grid

    def test_find_line(self):
        grid = [['.','.','a','b'],['a','.','b','.'],['a','a','.','a']]
        word = 'bo'
        # print find_line(grid, word)
        assert find_line(grid, word) == [0]

    def test_find_columns(self):
        grid = [['a','.','.'],['a','.','.'],['.','.','a']]
        word = 'bo'
        assert find_columns(grid, word) == [1,2]

    def test_get_rl_diagonal(self):
        grid = [['.','.','.','l'],['.','.','a','.'],
                ['.','x','.','.'],['.','.','.','.']]
        print 'space'
        for line in grid:
            print ' '.join(line)
        print get_rl_diagonal(grid, 1, 2)
        assert get_rl_diagonal(grid, 0, 3) == ['l','a','x','.']
        assert get_rl_diagonal(grid, 1, 2) == ['a','x','.']
        assert get_rl_diagonal(grid, 3, 3) == ['.']
        assert get_rl_diagonal(grid, 2, 3) == ['.','.']
        assert get_rl_diagonal(grid, 0, 0) == ['.']

    def test_find_rl_diagonal(self):
        grid = [['.','.','.','.'],['.','.','.','.'],
                ['.','.','.','.'],['.','.','.','.']]
        word = 'hoof'
        rl_find_test = find_diagonals(grid, word, get_rl_diagonal)
        print rl_find_test
        assert rl_find_test == [[0,3]]




if __name__ == '__main__':
    unittest.main()
